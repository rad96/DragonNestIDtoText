﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;

namespace WinTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            checkversion();
        }

        private void FillTable()
        {
            string[] ParsedArray = File.ReadAllLines("uistring.xml");
            TableID.RowCount = ParsedArray.Length-2;
            for (int j = 2; j < ParsedArray.Length; j++)
            {
                Match Reg;
                string StringPattern = "<message mid=\"(.*)\"><!\\[CDATA\\[(.*)]]></message>";
                Reg = Regex.Match(ParsedArray[j], StringPattern);
                if (Reg.Success)
                {
                    TableID[0, j - 2].Value = Regex.Replace(ParsedArray[j], StringPattern, (Convert.ToString(Reg.Groups[1])));
                    TableID[1, j - 2].Value = Regex.Replace(ParsedArray[j], StringPattern, (Convert.ToString(Reg.Groups[2])));
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CheckFilesExist();
            string[] ParsedArray = File.ReadAllLines("uistring.xml");
            for (int j = 2; j < ParsedArray.Length; j++)
            {
                Match Reg;
                string StringPattern = "<message mid=\"(.*)\"><!\\[CDATA\\[(.*)]]></message>";
                Reg = Regex.Match(ParsedArray[j], StringPattern);
                if (Reg.Success)
                {
                    if (SearchingID.Text == Convert.ToString(Reg.Groups[1]))
                    {
                        StringInFile.Text = Convert.ToString(Reg.Groups[2]);
                        break;
                    }
                }
            }
        }

        private void UpdateCheckButton_Click(object sender, EventArgs e)
        {
            update();
        }

        private void checkversion()
        {
            if (File.Exists("uistring.xml") == false)
            {
                update();
            }
            if (File.Exists("version.cfg") == false)
            {
                update();
            }
            StreamReader changeform = new StreamReader("version.cfg");
            VersionBox.Text = changeform.ReadLine();
            changeform.Close();
        }

        private void update()
        {
            WebClient webClient = new WebClient();
            if (File.Exists("version.cfg") == false)
            {
                StringInFile.Text = "Проверка наличия новой версии";
                webClient.DownloadFile("http://rad96.github.io/uistrings/version.cfg", "version.cfg");
                update();
            }
            else
            {
                StreamReader existverfile = new StreamReader("version.cfg");
                int existversion = Convert.ToInt32(existverfile.ReadLine());
                existverfile.Close();
                webClient.DownloadFile("http://rad96.github.io/uistrings/version.cfg", "versionnew.cfg");
                StreamReader newverfile = new StreamReader("versionnew.cfg");
                int newversion = Convert.ToInt32(newverfile.ReadLine());
                newverfile.Close();
                if (newversion > existversion)
                {
                    StringInFile.Text = "Пожалуйста, подождите. Идет обновление текстовой базы";
                    File.Delete("uistring.xml");
                    webClient.DownloadFile("http://rad96.github.io/uistrings/uistring.xml", "uistring.xml");
                    File.Delete("version.cfg");
                    File.Move("versionnew.cfg", "version.cfg");
                }
                else
                {
                    File.Delete("versionnew.cfg");
                }
            }
            if (File.Exists("uistring.xml") == false)
            {
                webClient.DownloadFile("http://rad96.github.io/uistrings/uistring.xml", "uistring.xml");
            }
            StreamReader changeform = new StreamReader("version.cfg");
            VersionBox.Text = changeform.ReadLine();
            StringInFile.Text = "Обновление завершено!";
            changeform.Close();
        }

        private void CheckFilesExist()
        {
            if (File.Exists("version.cfg") == false)
            {
                update();
            }
            if (File.Exists("uistring.xml") == false)
            {
                update();
            }
        }

        private void ButtonFillTable_Click_1(object sender, EventArgs e)
        {
            FillTable();
        }
    }
}
