﻿namespace WinTest
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Search = new System.Windows.Forms.Button();
            this.StringInFile = new System.Windows.Forms.RichTextBox();
            this.SearchingID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.UpdateCheckButton = new System.Windows.Forms.Button();
            this.VersionLabel = new System.Windows.Forms.Label();
            this.VersionBox = new System.Windows.Forms.TextBox();
            this.TableID = new System.Windows.Forms.DataGridView();
            this.Номер = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Текст = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ButtonFillTable = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TableID)).BeginInit();
            this.SuspendLayout();
            // 
            // Search
            // 
            this.Search.Location = new System.Drawing.Point(833, 95);
            this.Search.Name = "Search";
            this.Search.Size = new System.Drawing.Size(176, 42);
            this.Search.TabIndex = 0;
            this.Search.Text = "Поиск";
            this.Search.UseVisualStyleBackColor = true;
            this.Search.Click += new System.EventHandler(this.button1_Click);
            // 
            // StringInFile
            // 
            this.StringInFile.Location = new System.Drawing.Point(833, 191);
            this.StringInFile.Name = "StringInFile";
            this.StringInFile.Size = new System.Drawing.Size(304, 96);
            this.StringInFile.TabIndex = 1;
            this.StringInFile.Text = "";
            // 
            // SearchingID
            // 
            this.SearchingID.Location = new System.Drawing.Point(833, 54);
            this.SearchingID.Name = "SearchingID";
            this.SearchingID.Size = new System.Drawing.Size(176, 20);
            this.SearchingID.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.46F);
            this.label1.Location = new System.Drawing.Point(830, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Введите ID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.46F);
            this.label2.Location = new System.Drawing.Point(830, 157);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Текст, соответсвующий ID:";
            // 
            // UpdateCheckButton
            // 
            this.UpdateCheckButton.ForeColor = System.Drawing.Color.Black;
            this.UpdateCheckButton.Location = new System.Drawing.Point(1037, 95);
            this.UpdateCheckButton.Name = "UpdateCheckButton";
            this.UpdateCheckButton.Size = new System.Drawing.Size(100, 41);
            this.UpdateCheckButton.TabIndex = 6;
            this.UpdateCheckButton.Text = "Проверить обновления";
            this.UpdateCheckButton.UseVisualStyleBackColor = true;
            this.UpdateCheckButton.Click += new System.EventHandler(this.UpdateCheckButton_Click);
            // 
            // VersionLabel
            // 
            this.VersionLabel.AutoSize = true;
            this.VersionLabel.Location = new System.Drawing.Point(1034, 25);
            this.VersionLabel.Name = "VersionLabel";
            this.VersionLabel.Size = new System.Drawing.Size(94, 13);
            this.VersionLabel.TabIndex = 7;
            this.VersionLabel.Text = "Текущая версия:";
            // 
            // VersionBox
            // 
            this.VersionBox.Location = new System.Drawing.Point(1037, 54);
            this.VersionBox.Name = "VersionBox";
            this.VersionBox.ReadOnly = true;
            this.VersionBox.Size = new System.Drawing.Size(100, 20);
            this.VersionBox.TabIndex = 8;
            // 
            // TableID
            // 
            this.TableID.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TableID.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Номер,
            this.Текст});
            this.TableID.Location = new System.Drawing.Point(12, 25);
            this.TableID.Name = "TableID";
            this.TableID.Size = new System.Drawing.Size(796, 368);
            this.TableID.TabIndex = 9;
            // 
            // Номер
            // 
            this.Номер.HeaderText = "Номер";
            this.Номер.Name = "Номер";
            // 
            // Текст
            // 
            this.Текст.HeaderText = "Текст";
            this.Текст.Name = "Текст";
            this.Текст.Width = 650;
            // 
            // ButtonFillTable
            // 
            this.ButtonFillTable.Location = new System.Drawing.Point(833, 322);
            this.ButtonFillTable.Name = "ButtonFillTable";
            this.ButtonFillTable.Size = new System.Drawing.Size(295, 44);
            this.ButtonFillTable.TabIndex = 10;
            this.ButtonFillTable.Text = "Заполнить таблицу";
            this.ButtonFillTable.UseVisualStyleBackColor = true;
            this.ButtonFillTable.Click += new System.EventHandler(this.ButtonFillTable_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1149, 430);
            this.Controls.Add(this.ButtonFillTable);
            this.Controls.Add(this.TableID);
            this.Controls.Add(this.VersionBox);
            this.Controls.Add(this.VersionLabel);
            this.Controls.Add(this.UpdateCheckButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SearchingID);
            this.Controls.Add(this.StringInFile);
            this.Controls.Add(this.Search);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Текст от ID";
            ((System.ComponentModel.ISupportInitialize)(this.TableID)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Search;
        private System.Windows.Forms.RichTextBox StringInFile;
        private System.Windows.Forms.TextBox SearchingID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button UpdateCheckButton;
        private System.Windows.Forms.Label VersionLabel;
        private System.Windows.Forms.TextBox VersionBox;
        private System.Windows.Forms.DataGridView TableID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Номер;
        private System.Windows.Forms.DataGridViewTextBoxColumn Текст;
        private System.Windows.Forms.Button ButtonFillTable;
    }
}

